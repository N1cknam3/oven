import './users-list-com.html';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { TAPi18n } from 'meteor/tap:i18n';

Template.usersListCom.onCreated(function () {
  this.subscribe('users.all');
});

Template.usersListCom.helpers({
  users () {
    return Meteor.users.find();
  },
  getRolesText () {
    if (this.roles && this.roles.oven.length > 0) {
      return this.roles.oven.map(function (role) {
        return TAPi18n.__(role);
      }).join(', ');
    } else {
      return 'Пользователь';
    }
  }
});
