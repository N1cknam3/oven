import './users-list.html';
import { Template } from 'meteor/templating';
import '../../components/users/users-list-com.js';
import '../../components/common/page-heading/page-heading.js';
import { TAPi18n } from 'meteor/tap:i18n';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.usersList.helpers({
  getTitle () {
    return TAPi18n.__("Users list");
  },
  getLinks () {
    return [
      { label: TAPi18n.__("Home"), url: FlowRouter.path('App.home') },
      { label: TAPi18n.__("Users") }
    ];
  }
});
