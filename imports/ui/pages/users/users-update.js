import './users-update.html';
import { Template } from 'meteor/templating';
import '../../components/users/users-update-form-com.js';
import '../../components/common/page-heading/page-heading.js';
import { TAPi18n } from 'meteor/tap:i18n';
import { FlowRouter } from 'meteor/kadira:flow-router';

Template.usersUpdate.helpers({
  getUserId () {
    return FlowRouter.getParam('userId');
  },
  getTitle () {
    return TAPi18n.__("Edit user");
  },
  getLinks () {
    return [
      { label: TAPi18n.__("Home"), url: FlowRouter.path('App.home') },
      { label: TAPi18n.__("Users"), url: FlowRouter.path('App.users.list') },
      { label: TAPi18n.__("Edit") }
    ];
  }
});
