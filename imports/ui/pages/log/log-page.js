import './log-page.html';
import '../../components/log/log-list.js';
import '../../components/common/page-heading/page-heading.js';
import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { TAPi18n } from 'meteor/tap:i18n';
import { moment } from 'meteor/momentjs:moment';
import { ReactiveVar } from 'meteor/reactive-var';

Template.logPage.onCreated(function() {
  this.selectedDate = new ReactiveVar(moment().format('YYYY-MM-DD'));
});

Template.logPage.helpers({
  getTitle () {
    return TAPi18n.__("User actions log");
  },
  getLinks () {
    return [
      { label: TAPi18n.__("Home"), url: FlowRouter.path('App.home') },
      { label: TAPi18n.__("Log") }
    ];
  },
  getDate () {
    return moment().format('YYYY-MM-DD');
  },
  getSelectedDate() {
    let templateInstance = Template.instance();
    return templateInstance.selectedDate;
  }
});

Template.logPage.onRendered(function() {
  let templateInstance = Template.instance();
  $('#data_1 .input-group.date').datepicker({
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true,
    language: 'ru'
  }).on('changeDate', function (e) {
    templateInstance.selectedDate.set(moment(e.date).format('YYYY-MM-DD'));
  });
});