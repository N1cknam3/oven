import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

Accounts.validateNewUser(function (user) {
  var loggedInUser = Meteor.user();

  if (Roles.userIsInRole(loggedInUser, ['admin'], 'oven')) {
    return true;
  }

  throw new Meteor.Error(403, "Not authorized to create new users");
});
