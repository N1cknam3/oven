import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Roles } from 'meteor/alanning:roles';
import { Tracker } from 'meteor/tracker';

// Import needed templates
import '../../ui/layouts/body/body.js';
import '../../ui/layouts/blank/blank.js';
import '../../ui/pages/home/home.js';
import '../../ui/pages/not-found/not-found.js';
import '../../ui/pages/login/login.js';
import '../../ui/pages/users/users-list.js';
import '../../ui/pages/users/users-add.js';
import '../../ui/pages/users/users-update.js';
import '../../ui/pages/log/log-page.js';
import '../../ui/pages/ingredients/ingredients-list.js';
import '../../ui/pages/ingredients/ingredients-add.js';

FlowRouter.wait();

let allUsers = FlowRouter.group();
let managerUsers = FlowRouter.group({
  triggersEnter: [ function (context, redirect) {
    if (!Meteor.loggingIn() && !Meteor.userId()) {
      redirect('App.login')
    }
  } ]
});
let adminUsers = FlowRouter.group({
  triggersEnter: [ function (context, redirect) {
    if (!Meteor.loggingIn() && !Meteor.userId()) {
      redirect('App.login');
    } else {
      var loggedInUser = Meteor.user();

      if (!loggedInUser || !Roles.userIsInRole(loggedInUser, ['admin'], 'oven')) {
        redirect('App.notFound');
      }
    }
  } ]
});

// Set up all routes in the app
managerUsers.route('/', {
  name: 'App.home',
  action() {
    BlazeLayout.render('App_body', { content: 'App_home' });
  },
});

allUsers.route('/notFound', {
  name: 'App.notFound',
  action() {
    BlazeLayout.render('App_body', { content: 'App_notFound' });
  },
});

allUsers.route('/login', {
  name: 'App.login',
  action: function() {
    BlazeLayout.render('blankLayout', {content: 'login'});
  }
});

adminUsers.route('/users/list', {
  name: 'App.users.list',
  action: function() {
    BlazeLayout.render('App_body', {content: 'usersList'});
  }
});

adminUsers.route('/users/add', {
  name: 'App.users.add',
  action: function() {
    BlazeLayout.render('App_body', {content: 'usersAdd'});
  }
});

adminUsers.route('/ingredients/list', {
  name: 'App.ingredients.list',
  action: function() {
    BlazeLayout.render('App_body', {content: 'ingredientsList'});
  }
});

adminUsers.route('/ingredients/add', {
  name: 'App.ingredients.add',
  action: function() {
    BlazeLayout.render('App_body', {content: 'ingredientsAdd'});
  }
});

adminUsers.route('/users/update/:userId', {
  name: 'App.users.update',
  action: function() {
    BlazeLayout.render('App_body', {content: 'usersUpdate'});
  }
});

adminUsers.route('/log', {
  name: 'App.log',
  action: function() {
    BlazeLayout.render('App_body', {content: 'logPage'});
  }
});

Tracker.autorun(function() {
  if (Roles.subscription.ready()) {
     FlowRouter.initialize();
  }
});