// Import client startup through a single index entry point

import '../both/simple-schema-messages.js';
import './routes.js';
import './toasts.js';
import './i18n.js';
import './datepicker.js';