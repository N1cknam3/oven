import { Tracker } from 'meteor/tracker';
import { TAPi18n } from 'meteor/tap:i18n';
import SimpleSchema from 'simpl-schema';

export const usersUpdateFormSchema = new SimpleSchema({
  _id: {
    type: String,
    autoform: {
      type: 'hidden'
    }
  },
  name: {
      type: String,
      label: () => { return TAPi18n.__("User fullname") },
      max: 50
  },
  email: {
      type: String,
      autoform: {
        disabled: true
      },
      optional: true,
      regEx: SimpleSchema.RegEx.Email,
      label: () => { return TAPi18n.__("E-mail address") }
  },
  password: {
    type: String,
    label: () => { return TAPi18n.__("Enter a password") },
    autoform: {
      type: "password"
    },
    optional: true,
    min: 4,
  },
  confirmPassword: {
    type: String,
    label: () => { return TAPi18n.__("Enter the password again") },
    autoform: {
      type: "password"
    },
    optional: true,
    min: 4,
    custom() {
      if (this.value !== this.field('password').value) {
        return TAPi18n.__("passwordMismatch");
      }
    }
  },
  roles: {
    optional: true,
    type: Array,
    label: () => { return TAPi18n.__("Roles") },
    autoform: {
      type: "select-checkbox",
      options: [
        { label: 'Диспетчер', value: 'dispatcher' },
        { label: 'Администратор', value: 'admin' }
      ]
    }
  },
    'roles.$': {
      type: String,
      allowedValues: ['admin', 'dispatcher']
    }
}, { tracker: Tracker });
