import { Logger } from 'meteor/ostrio:logger';
import { LoggerMongo } from 'meteor/ostrio:loggermongo';
import { Meteor } from 'meteor/meteor';

export  const OvenLog = new Mongo.Collection('ovenLog');
export const log = new Logger();

(new LoggerMongo(log, {
  collection: OvenLog
})).enable();